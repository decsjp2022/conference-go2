import json
import pika
import django
import os
import sys
from django.core.mail import send_mail
from pika.exceptions import AMQPConnectionError
import time

sys.path.append("")
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "presentation_mailer.settings")
django.setup()


def approval_message(ch, method, properties, body):
    print("Received approved presentation")
    content = json.loads(body)
    presenter_name = content["presenter_name"]
    presenter_email = content["presenter_email"]
    title = content["title"]
    subject = "Your presentation has been accepted"
    message = f"{presenter_name}, we are happy to tell you that your presentation, {title}, has been accpeted"
    send_mail(
        subject,
        message,
        "admin@conferencego",
        [presenter_email],
        fail_silently=False,
    )


def rejection_message(ch, method, properties, body):
    print("Received approved presentation")
    content = json.loads(body)
    presenter_name = content["presenter_name"]
    presenter_email = content["presenter_email"]
    title = content["title"]
    subject = "Your presentation has been rejected"
    message = f"{presenter_name}, we are sorry to inform you that your presentation, {title}, has been rejected."
    send_mail(
        subject,
        message,
        "admin@conferencego",
        [presenter_email],
        fail_silently=False,
    )


def main():
    parameters = pika.ConnectionParameters(host="rabbitmq")
    connection = pika.BlockingConnection(parameters)
    channel = connection.channel()
    channel.queue_declare(queue="presentation_approvals")
    channel.queue_declare(queue="presentation_rejections")
    channel.basic_consume(
        queue="presentation_approvals",
        on_message_callback=approval_message,
        auto_ack=True,
    )
    channel.basic_consume(
        queue="presentation_rejections",
        on_message_callback=rejection_message,
        auto_ack=True,
    )
    print(" [*] Waiting for messages")
    channel.start_consuming()


while True:
    try:
        if __name__ == "__main__":
            try:
                main()
            except KeyboardInterrupt:
                print("Interrupted")
                try:
                    sys.exit(0)
                except SystemExit:
                    os._exit(0)
    except AMQPConnectionError:
        print("Could not connect to RabbitMQ")
        time.sleep(5.0)
